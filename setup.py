import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="GlobusTransferHelper",
    version="0.1.0",
    author="Roland Haas",
    author_email="rhaas@ncsa.illinois.edu",
    description="A convenience wrapper around Globus' Python API",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ncsa.illinois.edu/bw-seas/globustransferhelper",
    py_modules=["globustransferhelper"],
    scripts=["showcase_helper.py"],
    classifiers=[
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: NCSA License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7',
    install_requires=["globus-sdk"],
)
