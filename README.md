GlobusTransferHelper
====================

What is this?
---------------

GlobusTransferHelper is a utility python package to be used with the Python API
to [Globus](http://globus-sdk-python.readthedocs.io/en/latest/). It aims to 
simplify common tasks and reduce the number of cide lines needed for simple
tasks.

Installation
------------

You can use pip wiht this repository to install the package:
```#bash
pip install git+https://git.ncsa.illinois.edu/bw-seas/globustransferhelper
```

Usage
-----

When used as a python (2 or 3) module the helper is used like this:

```#python
import globustransferhelper

hlp = globustransferhelper.GlobusTransferHelper()

files = hlp.ls(globustransferhelper.EP_BLUEWATERS, "~")
print (files.keys())
```

The module can also be called from the command line, in which case it will log
you in to Globus and activate the BlueWaters and NearLine endpoints.
```#bash
./globustransferhelper.py
```

Examples
--------

The file `showcase_helper.py` in this repository demonstrates most functionality
in the code.

Authors
-------

* the Blue Waters SEAS team
* Galen Arnold
* Roland Haas

TODO
----

1. add more useful python docstrings
