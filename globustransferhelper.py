#!/usr/bin/env python

# Copyright (c) 2019 The Board of Trustees of the University of Illinois
# All rights reserved.
#
# Developed by: National Center for Supercomputing Applications
#               University of Illinois at Urbana-Champaign
#               http://www.ncsa.illinois.edu/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal with the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimers.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimers in the documentation
# and/or other materials provided with the distribution.
#
# Neither the names of the National Center for Supercomputing Applications,
# University of Illinois at Urbana-Champaign, nor the names of its
# contributors may be used to endorse or promote products derived from this
# Software without specific prior written permission.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# WITH THE SOFTWARE.

"""
Based on Galen Arnold's test mule for jenkins monitor
Based on tutorial and documentation at:
   http://globus.github.io/globus-sdk-python/index.html

Stores cached authentication tokens in
$HOME/.globustransferhelper/refresh-tokens.json

Roland Haas, 2018, NCSA
Galen Arnold, 2018, NCSA
"""
import os
import errno
import globus_sdk
import json
from collections import OrderedDict

# endpoints determined by globus cli: globus endpoint search ncsa#bluewaters
# or from globus.org -> "Manage Endpoints" -> endpoint detail, UUID
EP_BLUEWATERS = "d59900ef-6d04-11e5-ba46-22000b92c6ec"
EP_BLUEWATERS_DUO = "fccadd10-dafc-11e9-80d3-0adb8449f244"
EP_BLUEWATERS_AWS = "c518feba-2220-11e8-b763-0ac6873fc732"
EP_BLUEWATERS_GOOGLE = "36b243a0-6d5d-11e9-8e59-029d279f7e24"

# TODO: turn into a full fledged file-system-content object that can handle
# globus and local file systems transparently (needs some caching to be
# useful)?
class GlobusTransferHelper:
    # TODO: move generic routines (for globus client) into base class
    tclient = None
    TIMEOUT = 60
    # this ID uniquely identifies the script to Globus together with the
    # access token it lets the script act on the user's behalf
    CLIENT_ID = 'c87acef9-2bac-4743-90e8-2ae9e999ff29'
    # TODO add globus magic strings as constants
    def __init__(self, interactive=True):
        """interactive - some screen output, ask for input if needed"""
        self.interactive = interactive

        REDIRECT_URI = 'https://auth.globus.org/v2/web/auth-code'
        SCOPES = 'openid email profile ' \
                 'urn:globus:auth:scope:transfer.api.globus.org:all'
        # we store the access tokens in this file. TODO: store in some central database
        # These are equivalent to an ssh private key so must *not* be shared.
        TOKEN_FILE = '%s/.globustransferhelper/refresh-tokens.json' % os.environ['HOME']
        try:
            # if we already have tokens, load and use them
            tokens = self.load_tokens_from_file(TOKEN_FILE)
        except:
            # if we still need to get tokens, start the Native App authentication process
            if self.interactive:
                tokens = self.do_native_app_authentication(self.CLIENT_ID, REDIRECT_URI, SCOPES)
            else:
                raise
        try:
            self.save_tokens_to_file(TOKEN_FILE, tokens)
        except:
            pass
        transfer_tokens = tokens['transfer.api.globus.org']
        auth_client = globus_sdk.NativeAppAuthClient(client_id=self.CLIENT_ID)
        def refresh_callback(token_response):
            self.update_tokens_file_on_refresh(TOKEN_FILE, token_response)
        authorizer = globus_sdk.RefreshTokenAuthorizer(
            transfer_tokens['refresh_token'],
            auth_client,
            access_token=transfer_tokens['access_token'],
            expires_at=transfer_tokens['expires_at_seconds'],
            on_refresh=refresh_callback)
        self.tclient = globus_sdk.TransferClient(authorizer=authorizer)

    def input(self, prompt=None):
        """call input function appropriate for python 2/3"""
        try:
            return raw_input(prompt)
        except NameError:
            return input(prompt)

    def isstring(self, s):
        """find out if s is a string"""
        try:
            stringclass = basestring
        except NameError:
            stringclass = str
        return isinstance(s, stringclass)

    # generic Globus REST API helpers
    def load_tokens_from_file(self, filepath):
        """Load a set of saved tokens."""
        with open(filepath, 'r') as tokenfile:
            tokens = json.load(tokenfile)
        return tokens
 
    def save_tokens_to_file(self, filepath, tokens):
        """Save a set of tokens for later use."""
        try:
            os.makedirs(os.path.dirname(filepath))
        except OSError as e:
            if e.errno == errno.EEXIST:
                pass
            else:
                raise
        with open(filepath, 'w') as tokenfile:
            json.dump(tokens, tokenfile)

    def update_tokens_file_on_refresh(self, TOKEN_FILE, token_response):
        """
        Callback function passed into the RefreshTokenAuthorizer.
        Will be invoked any time a new access token is fetched.
        """
        self.save_tokens_to_file(TOKEN_FILE, token_response.by_resource_server)

    def do_native_app_authentication(self, client_id, redirect_uri,
                                     requested_scopes=None):
        """
        Does a Native App authentication flow and returns a
        dict of tokens keyed by service name.
        """
        print ("client: ",client_id)
        client = globus_sdk.NativeAppAuthClient(client_id=client_id)
        # pass refresh_tokens=True to request refresh tokens
        client.oauth2_start_flow(requested_scopes=requested_scopes,
                                 redirect_uri=redirect_uri,
                                 refresh_tokens=True)
        url = client.oauth2_get_authorize_url()
        print('Native App Authorization URL: \n{0}'.format(url))
        auth_code = self.input('Enter the auth code: ').strip()
        token_response = client.oauth2_exchange_code_for_tokens(auth_code)
        # return a set of tokens, organized by resource server name
        return token_response.by_resource_server

    def get_transfer_client(self):
        """Return the globus TransferClient object used"""
        return self.tclient

    def endpoint_autoactivate(self, endpoint):
        """present URL to activate an endpoint if needed
        Input:
        endpoint - globus endpoint UUID on which files should be deleted
        Returns:
        True if activation succeeded
        """
        r = self.tclient.endpoint_autoactivate(endpoint, if_expires_in=24*3600)
        while (r["code"] == "AutoActivationFailed"):
            if(self.interactive):
                print("Endpoint requires manual activation, please open "
                      "the following URL in a browser to activate the "
                      "endpoint:")
                print("https://app.globus.org/file-manager?origin_id=%s"
                      % endpoint)
                self.input("Press ENTER after activating the endpoint:")
                r = self.tclient.endpoint_autoactivate(endpoint, if_expires_in=24*3600)
        return r["code"].startswith("AutoActivated") or \
               r["code"].startswith("AlreadyActivated")

    # transfer API helpers
    def task_wait(self, result):
        """
        block on tclient task so that there's no overlap with the next transfer
        Inputs:
        result - the object returned by one of the asynchronous calls
        Returns:
        None
        """
        while not self.tclient.task_wait(result["task_id"], timeout=self.TIMEOUT):
            if self.interactive:
                print("Waiting on {0} to complete".format(result["task_id"]))
        return self.tclient.get_task(result["task_id"])

    def delete(self, endpoint, paths, recursive = False, synchronous = True,
               mylabel = None):
        """
        DeleteData call wrapper
        Inputs:
        endpoint - globus endpoint UUID on which files should be deleted
        paths - a single string or an iterable of paths to delete
        recursive - if a path is a directory, delete its content as well
                    (default: False)
        synchronous - wait until done (defatult: True)
        mylabel - any textual label to identify the task (default: None)
        Return:
        a globus TransferResponse object describing the transfer
        """
        ddata = globus_sdk.DeleteData(self.tclient, endpoint, label=mylabel,
                                      recursive=recursive)
          
        if self.isstring(paths):
            paths = [paths]
        for path in paths:
            ddata.add_item(path)
        delete_result = self.tclient.submit_delete(ddata)
        if synchronous:
            delete_result = self.task_wait(delete_result)
        return delete_result

    def transfer(self, srcpoint, destpoint, transfers,
                 recursive = False, sync_level = "checksum",
                 synchronous = False, mylabel = None):
        """
        TransferData call wrapper. Copies files between endpoints.
        Inputs:
        srcpoint - globus endpoint UUID of the source of the transfer
        destpoint - globus endpoint UUID of the destination of the transfer
        paths - an iterable of files and directories to transfer, each item
                itself is a 2 item list specifying the path on the source and
                on the destination
        recursive - if an object is a directory, delete its content as well
                    (default: False)
        sync_level - one of "checksum" (default), "exists", "size", "mtime"
        synchronous - wait until done (defatult: True)
        mylabel - any textual label to identify the task (default: None)
         
        Return:
        a globus TransferResponse object describing the transfer
        """
        tdata = globus_sdk.TransferData(self.tclient, srcpoint, destpoint,
                                        label=mylabel,
                                        sync_level=sync_level)
        for transfer in transfers:
            tdata.add_item(transfer[0], transfer[1], recursive=recursive)
        transfer_response = self.tclient.submit_transfer(tdata)
        if synchronous:
            transfer_response = self.task_wait(transfer_response)
        return transfer_response

    def ls(self, endpoint, path, depth=1):
        """
        Operation_ls call wrapper
        Inputs:
        endpoint - globus endpoint UUID on which files should be listed
        path - path to a file or directory
        depth - maximum depth to recurse when listing, 1 shows the content of
                the given directory, "infinite" recurses without bounds
                (default: 1) 
        Returns: an ordered dict with information about the found objects as
                 returned by the underlying Globus operation_ls. The dictorany
                 keys are the path of the object relative to the starting
                 point. Entries are intially sorted such that files in a
                 directory immediately follow the directory. Each entry is dict
                 with this content (see
                 https://docs.globus.org/api/transfer/file_operations/#list_directory_contents):
                 path - the absolute path of the object
                 name - the name of the object
                 type - one of "file", "dir" or "invalid_symlink"
                 link_target - the target of a symbolic link
                 permissions - the octal UNIX permission as a string
                 size - file size in bytes
                 user - the user owning the file or directory
                 group - the group owning the file or directory
                 last_modified - modification time, always UTC, format:
                                 YYYY-MM-DD HH:MM:SS+00:00
        """
        if(depth < 1 and depth != 'infinite'):
            raise ValueError('depth must be at least 1, given: %s' % str(depth))
        def real_ls(tclient, endpoint, path, depth, prefix):
            """
            Recursively called for each directory encountered.
            Inputs:
                tclient - the Globus transfer client
                endpoint - Globus endpoint to interact with
                path - directory to list
                depth - recurse down this many levels (level 1 is content of
                        the given directory)
                prefix - internal use, to create a full path relative to the
                         initial path in the returned list of entries
            Returns:
                an dict of entries as returned by Globus' operation_ls with
                the name being the full path relative to the the path argument
            """
            entries = tclient.operation_ls(endpoint, path=path)
            retval = OrderedDict()
            for entry in entries:
                full_path = path + "/" + entry['name']
                full_name = prefix + entry["name"]
                new_entry = entry.copy()
                new_entry['path'] = full_path
                retval[full_name] = new_entry
                if(depth > 1 or depth == 'infinite'):
                    if(entry['type'] == 'dir'):
                        new_depth = depth if depth == 'infinite' else depth-1
                        new_prefix = prefix + entry['name'] + "/"
                        # TODO: should I instead return a tree structure like the FS?
                        retval.update(real_ls(tclient, endpoint, path + "/" + entry['name'],
                                               new_depth, new_prefix))
            return retval
        return real_ls(self.tclient, endpoint, path, depth, '')

    def mkdir(self, endpoint, path, synchronous = False):
        """
        Create a directory
        Inputs:
        endpoint - globus endpoint UUID on which files should be listed
        path - the path of the directory to create
        synchronous - wait until done (defatult: True)
        Returns:
        a globus TransferResponse object
        """
        transfer_response = self.tclient.operation_mkdir(endpoint, path=path)
        if synchronous:
            transfer_response = self.task_wait(transfer_response)
        return transfer_response

    def rename(self, endpoint, oldpath, newpath, synchronous = False):
        """
        Rename a file or directory
        Inputs:
        endpoint - globus endpoint UUID on which files should be listed
        srcpath - the path of the object to rename
        newpath - the new path of the object
        synchronous - wait until done (defatult: True)
        Returns:
        a globus TransferResponse object
        """
        transfer_response = \
            self.tclient.operation_rename(endpoint, oldpath, newpath)
        if synchronous:
            transfer_response = self.task_wait(transfer_response)
        return transfer_response

if __name__ == '__main__':
    # log in, then quit. Useful for scripted use
    transfer = GlobusTransferHelper()
    transfer.endpoint_autoactivate(EP_BLUEWATERS_DUO)
