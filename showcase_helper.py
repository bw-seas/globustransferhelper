#!/usr/bin/env python

# Copyright (c) 2019 The Board of Trustees of the University of Illinois
# All rights reserved.
#
# Developed by: National Center for Supercomputing Applications
#               University of Illinois at Urbana-Champaign
#               http://www.ncsa.illinois.edu/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal with the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimers.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimers in the documentation
# and/or other materials provided with the distribution.
#
# Neither the names of the National Center for Supercomputing Applications,
# University of Illinois at Urbana-Champaign, nor the names of its
# contributors may be used to endorse or promote products derived from this
# Software without specific prior written permission.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# WITH THE SOFTWARE.

import os
import globustransferhelper as globushl

# create the helper, this logs you in
helper = globushl.GlobusTransferHelper()

# activate endpoints
success = helper.endpoint_autoactivate(globushl.EP_BLUEWATERS_DUO)
print("Activated BlueWaters: {0}".format(success))

# show the content of your $HOME on BlueWaters
print("Your $HOME contains (down to 2 levels):")
files = helper.ls(globushl.EP_BLUEWATERS_DUO, "~", depth=2)
for file in files:
    print("%s (%s) [%d bytes]" % \
         (file, files[file]['type'], files[file]['size']))

# create a file and copy it using globus
foxfile = "%s/%s-foxfile" % (os.getcwd(), os.environ["USER"])
with open(foxfile, "w") as fh:
    fh.write("The quick brown fox jumps over the lazy dog\n")

# duplicate on BW
houndfile = "%s/%s-houndfile" % (os.getcwd(), os.environ["USER"])
harefile = "%s/%s-harefile" % (os.getcwd(), os.environ["USER"])
print("Copying %s to BlueWaters %s and %s" % (foxfile, houndfile, harefile))
transfer = helper.transfer(globushl.EP_BLUEWATERS_DUO, globushl.EP_BLUEWATERS_DUO, 
                           [(foxfile, houndfile), (foxfile, harefile)])
helper.task_wait(transfer)
with open(houndfile, "r") as fh:
    print("Hounds contains:", fh.readlines())
print("done")

# rename file (can also move files using this)
kenneldir = "%s/%s-kennel" % (os.getcwd(), os.environ["USER"])
kennel_houndfile = os.path.join(kenneldir, os.path.split(houndfile)[-1])
kennel_foxfile = os.path.join(kenneldir, os.path.split(foxfile)[-1])
print("Moving %s, and %s into %s" % (foxfile,houndfile,kenneldir))
os.mkdir(kenneldir)
helper.rename(globushl.EP_BLUEWATERS_DUO, houndfile, kennel_houndfile)
helper.rename(globushl.EP_BLUEWATERS_DUO, foxfile, kennel_foxfile)
files = helper.ls(globushl.EP_BLUEWATERS_DUO, kenneldir)
print(list(files.keys()))

# get rid of files
print("Removing files")
deleted = helper.delete(globushl.EP_BLUEWATERS_DUO, [harefile, kenneldir], recursive=True)
print("Success: %s" % deleted['status'])

print("All done")
